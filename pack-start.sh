#!/bin/bash

PORT=$1
RETRY=5000 # default 5000
BASE_REACH=1200000  # default 1200000

if [ $PORT = "-h" ]
  then echo "Run this script as root

USAGE:
	sudo packet.sh PORT/OPTIONS

Options (Use instead of a port):
	-h  --  Display this message"
  exit
fi

if [ "$EUID" -ne 0 ]
  then echo "Please run as root!"
  exit 1
fi

killall kissattach
kissattach $PORT radio
if [ $? -eq 0 ]; then
  :
else
  echo failed to attach
  exit 1
fi

kissparms -p radio -t 300 -l 10 -s 12 -r 80 -f n
ifconfig ax0 10.10.10.11/26

echo $BASE_REACH > /proc/sys/net/ipv4/neigh/ax0/base_reachable_time_ms
echo $RETRY > /proc/sys/net/ipv4/neigh/ax0/retrans_time_ms

